
Create Database Ioasys;

GO

USE Ioasys;
GO


Create Table Empresa (
		Id int primary key Identity(1, 1),
		Nome varchar(100), 
		Tipo int, 
		Endereco varchar(255)
)

GO

Create Table AcessoUsuario(
Id int primary key Identity(1,1),
IdInvestor int,
AccessToken varchar(100),
Client varchar(100),
UID varchar(100)
)

GO
Insert Into Empresa Values('Empresa X', 1, 'Rua S�o Paulo, n� 100, Centro - Belo Horizonte')
Insert Into Empresa Values('Empresa Y', 1, 'Rua Rio de Janeiro, n� 456, Centro - Belo Horizonte')
Insert Into Empresa Values('Empresa Z', 2, 'Rua Curitiba, n� 100, Bairro A - Nova Lima')
