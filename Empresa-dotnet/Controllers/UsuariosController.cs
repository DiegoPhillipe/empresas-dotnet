﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Empresa_dotnet.Services;
using Empresa_dotnet.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Empresa_dotnet.Controllers
{
    [Route("api/[controller]")]
    public class UsuariosController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly IUsuariosService _usuariosService;

        public UsuariosController(ApplicationDbContext context, IUsuariosService usuariosService)
        {
            _context = context;
            _usuariosService = usuariosService;
        }

        [HttpPost()]
        [Route("sign_in")]
        public async Task<ActionResult> sign_in(SignInModel model)
        {
            try
            {
                var userAccess = await _usuariosService.SignIn(model.Email, model.Password);

                if (userAccess == null)
                    return Unauthorized();

                return Ok(userAccess);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
