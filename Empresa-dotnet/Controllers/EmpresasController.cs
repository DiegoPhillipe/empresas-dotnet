﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Empresa_dotnet.Services;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Empresa_dotnet.Controllers
{
    [Route("api/[controller]")]
    public class EmpresasController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly IEmpresasService _empresasService;

        public EmpresasController(ApplicationDbContext context, IEmpresasService empresasServices)
        {
            _context = context;
            _empresasService = empresasServices;
        }

        [AuthorizeIOSYS]
        [HttpGet("{id}")]
        public IActionResult Get(int id) => Ok(_empresasService.Get(id));


        [AuthorizeIOSYS]
        [HttpGet()]
        public IActionResult Get(int? tipo_empresa, string name) => Ok(_empresasService.Get(tipo_empresa, name));

        public class AuthorizeIOSYS : Attribute, IResourceFilter
        {
            public void OnResourceExecuting(ResourceExecutingContext context)
            {
                var userServices = (IUsuariosService)context.HttpContext.RequestServices.GetService(typeof(IUsuariosService));

                StringValues accessToken = new StringValues();
                context.HttpContext.Request.Headers.TryGetValue("access-token", out accessToken);

                StringValues client = new StringValues();
                context.HttpContext.Request.Headers.TryGetValue("client", out client);

                StringValues uid = new StringValues();
                context.HttpContext.Request.Headers.TryGetValue("uid", out uid);

                var userValid = userServices.ValidateUser(accessToken.FirstOrDefault(), client.FirstOrDefault(), uid.FirstOrDefault());

                if (!userValid)
                    context.Result = new UnauthorizedResult();

            }

            public void OnResourceExecuted(ResourceExecutedContext context)
            {
            }
        }

    }
}
