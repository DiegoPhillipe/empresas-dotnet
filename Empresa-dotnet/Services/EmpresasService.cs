﻿using Empresa_dotnet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresa_dotnet.Services
{
    public class EmpresasService : IEmpresasService
    {
        private readonly ApplicationDbContext _context;

        public EmpresasService(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Empresa> Get(int? tipo, string nome)
        {

            var asQueryable = _context.Empresa.AsQueryable();

            if (!string.IsNullOrEmpty(nome))
                asQueryable = asQueryable.Where(m => m.Nome == nome);

            if (tipo.HasValue)
                asQueryable = asQueryable.Where(m => m.Tipo == tipo);


            return asQueryable.ToList();
        }

        public Empresa Get(int id)
        {
            return _context.Empresa.Where(c => c.Id == id).FirstOrDefault();
        }
    }
}
