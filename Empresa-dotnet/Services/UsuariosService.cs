﻿using Empresa_dotnet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Empresa_dotnet.Services
{
    public class UsuariosService : IUsuariosService
    {
        private readonly ApplicationDbContext _context;

        public UsuariosService(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool ValidateUser(string accessToken, string client, string uid)
        {
            var userValid = _context.AcessoUsuario.Where(m => m.Client == client && m.AccessToken == accessToken && m.UID == uid).FirstOrDefault();

            if (userValid == null)
                return false;

            return true;
        }

        public async Task<AcessoUsuario> SignIn(string email, string password)
        {
            var httpClient = new HttpClient();

            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("http://empresas.ioasys.com.br/api/v1/users/auth/sign_in"),
                Content = new StringContent($@"{{""email"":""{email}"",""password"":""{password}""}}", Encoding.UTF8, "application/json")
            };

            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await httpClient.SendAsync(request);

            // Check login success
            if (response.IsSuccessStatusCode)
            {
                var investorData = new InvestorData();
                using (HttpContent content = response.Content)
                {
                    var json = content.ReadAsStringAsync().Result;
                    investorData = Newtonsoft.Json.JsonConvert.DeserializeObject<InvestorData>(json);
                }
                if (investorData.Success)
                {
                    var user = new AcessoUsuario();

                    user.AccessToken = response.Headers.GetValues("access-token").FirstOrDefault();
                    user.Client = response.Headers.GetValues("client").FirstOrDefault();
                    user.UID = response.Headers.GetValues("uid").FirstOrDefault();
                    user.IdInvestor = investorData.Investor.Id;

                    // Check header is not empty
                    if (!string.IsNullOrEmpty(user.AccessToken) && !string.IsNullOrEmpty(user.Client) && !string.IsNullOrEmpty(user.UID))
                    {
                        // Incluir no banco de dados ...
                        _context.AcessoUsuario.Add(user);
                        _context.SaveChanges();

                        return new AcessoUsuario
                        {
                            AccessToken = user.AccessToken,
                            Client = user.Client,
                            UID = user.UID
                        };
                    }
                }
            }

            return null;
        }
    }
}
