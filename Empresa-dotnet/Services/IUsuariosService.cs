﻿using Empresa_dotnet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresa_dotnet.Services
{
    public interface IUsuariosService
    {
        Task<AcessoUsuario> SignIn(string email, string password);
        bool ValidateUser(string accessToken, string client, string uid);
    }
}
