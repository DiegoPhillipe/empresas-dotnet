﻿using Empresa_dotnet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresa_dotnet.Services
{
    public interface IEmpresasService
    {
        List<Empresa> Get(int? tipo, string nome);
        Empresa Get(int id);
    }
}
