﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Empresa_dotnet.Models
{
    public class Empresa
    {
        [Key]
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Tipo { get; set; }
        public string Endereco { get; set; }
    }
}
