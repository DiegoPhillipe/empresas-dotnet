﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Empresa_dotnet.Models
{
    public class AcessoUsuario
    {
        [Key]
        public int Id { get; set; }
        public int IdInvestor { get; set; }
        public string AccessToken { get; set; }
        public string Client { get; set; }
        public string UID { get; set; }
    }
}
