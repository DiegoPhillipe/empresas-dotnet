﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresa_dotnet.Models
{
    public class Investor
    {
        public int Id { get; set; }
    }

    public class InvestorData
    {

        public Investor Investor { get; set; }
        public bool Success { get; set; }
    }
}
